#!/usr/bin/env ksh
# =============================================================================
# Header_start
# =============================================================================
# Description	:
#
#	Script de controle de l'applicatif (multi-serveurs)
#
# -----------------------------------------------------------------------------
# Fichier de configuration :
#
#	OperateAppli.conf
#
# Format des lignes dans la CONF :
#
#	Champ 1 :   2  :  3  :  4 :    5   :  6 :     7    :    8    :    9 
# 	GR_FONCT:ACTION:ETAPE:USER:COMMANDE:HOST:ETAPE_SUIV:ETAPE_ERR:RETOUR_OK
#
# -----------------------------------------------------------------------------
# Auteur	: Axel ROGER
# Date creation	: 2019-05-27
# Charge(JA)	: 5
# -----------------------------------------------------------------------------
# Revisions :
#
#	S0F0	| 2019-05-27 | knbv5822 | Version initiale
#	S0F1	| 2019-11-19 | knbv5822 | Suppression de la substitution ${!xxx} pour compatibilite
#	S0F2	| 2019-11-21 | knbv5822 | Remplacement des commandes <<< et realpath pour compatibilite
#	S0F3	| 2019-12-06 | knbv5822 | Casse ignoree - Execution unique autorisee
#	S0F4	| 2020-01-09 | knbv5822 | Correction des gestions de CR
#	S0F5	| 2020-02-03 | knbv5822 | Bug d'upload sur correction S0F2
#	S0F6	| 2020-02-04 | knbv5822 | Prise en compte des noms d'hote en -adm
#
# =============================================================================
# Header_end
# =============================================================================
# set -x		# Decommenter pour DEBUG
# exec 2>/dev/null	# Decommenter pour inhiber la sortie d'erreur

# =============================================================================
# ================= FONCTIONS =================================================
# =============================================================================

# ----------------- Fonction d'ecriture logs ----------------------------------
#
gLog() {
	while IFS='' read -r IN; do
		if [ -n "$IN" ] && [ ` echo "$IN" | egrep -c "^==" ` -eq 0 ]; then
			IN="${c_norm}` date '+%d/%m/%Y %T' ` $IN"
		fi
		echo $IN
		echo $IN >> $LOGFILE
	done
}

# ----------------- Affichage de l'aide ---------------------------------------
#
usage() {
echo "
Usage: $0 [OPTIONS] ACTION GR_FONCT

Effectue une ACTION sur le groupe fonctionnel GR_FONCT applicatif
tel que defini dans le fichier de configuration.

Version: $VERSION

[OPTIONS]
  -c		Active les couleurs
  -v		Mode verbeux
  -V		Affiche la version du script et s'arrete
  -t		Mode tests uniquement (pas d'action)
  -f		Mode force (prerequis ignores)
  -h		Affiche cette aide et s'arrete

[ACTION]
  START         Demarrage
  STOP          Arret
  STATUS        Statut
  RESTART       Arret puis Demarrage
  ...           Voir le fichier de configuration

Fichier de configuration: $CONFFILE

Envoyer les rapports de bugs a xxx@orange.com
Documentation: http://kiwi.sso.francetelecom.fr/index.php/FEX_OperateAppli
"
}

# ----------------- Initialisation des couleurs -------------------------------
#
initFormat() {
	if $COLORS && [ -t 1 ] && [ -n $( tput colors 2>/dev/null ) ] && [ $( tput colors 2>/dev/null ) -ge 8 ]; then
		c_bleu=` tput setf 1`
		c_vert=` tput setf 2`
		c_cyan=` tput setf 3`
		c_roug=` tput setf 4`
		c_rose=` tput setf 5`
		c_jaun=` tput setf 6`
		c_norm=` tput sgr0 `
		OK="${c_vert}OK${c_norm}"
		KO="${c_roug}KO${c_norm}"
		$STATUS && SUCCES="${c_vert}DEMARRE${c_norm}" || SUCCES="${c_vert}SUCCES${c_norm}"
		$STATUS && ECHEC="${c_roug}ARRETE${c_norm}" || ECHEC="${c_roug}ECHEC${c_norm}"
		ERREUR="${c_roug}ERREUR${c_norm}"
		WARN="${c_jaun}WARN${c_norm}"
	else
		OK="OK"
		KO="KO"
		$STATUS && SUCCES="DEMARRE" || SUCCES="SUCCES"
		$STATUS && ECHEC="ARRETE" || ECHEC="ECHEC"
		ERREUR="ERREUR"
		WARN="WARN"
	fi
}

# ----------------- Stockage d'un host pour bypass ----------------------------
#
stockBypassHost() {
	HOST_TO_BYPASS="` echo "$1" | tr '[A-Z]' '[a-z]' `"

	# Si non, on le stock
	if ! ` checkBypassHost $HOST_TO_BYPASS ` ; then
		HOSTS_BYPASS_ARRAY[$nb_bypass]="$HOST_TO_BYPASS"
		nb_bypass=$(( $nb_bypass + 1 ))
	fi
}

# ----------------- Verification d'un host pour bypass ------------------------
#
checkBypassHost() {
	HOST_TO_CHECK="$1"

	# On verifie que le host est identifie pour bypass
        for host in ${HOSTS_BYPASS_ARRAY[@]}; do
                if [ "$host" = "$HOST_TO_CHECK" ]; then
                        echo true
			return 0
                fi
        done

	echo false
	return 1
}

# ----------------- Verification des serveurs cibles --------------------------
#
verifHostsCibles() {

	CR_CHECK_HOST=0

	for CHECK_HOST in $@; do
		
		CHECK_HOST_DISPLAY="${c_jaun}$CHECK_HOST${c_norm}"

		# 1: Verification de la connectivite SSH
		if [ ` su $SCRIPTUSER -c "ssh -nq $SCRIPTUSER@$CHECK_HOST ''" 2>/dev/null; echo $? ` -ne 0 ]; then
			echo "  $KO : $CHECK_HOST_DISPLAY -> Probleme de connexion SSH" | gLog
			CR_CHECK_HOST=1
		elif $VERBOSE; then
			echo "  $OK : $CHECK_HOST_DISPLAY -> Connection SSH valide" | gLog
		fi

		# 2a: Push du fichier de configuration (non bloquant)
		if [ ` su $SCRIPTUSER -c "scp -q $CONFFILE $SCRIPTUSER@$CHECK_HOST:$CONFFILE" 2>/dev/null; echo $? ` -ne 0 ]; then
			echo "  $WARN : $CHECK_HOST_DISPLAY -> Push du fichier $CONFFILE en echec" | gLog
		elif $VERBOSE; then
			echo "  $OK : $CHECK_HOST_DISPLAY -> Push du fichier $CONFFILE reussi" | gLog
		fi

		# 2b: Push du script (non bloquant)
		if [ ` su $SCRIPTUSER -c "scp -q $SCRIPTFILE $SCRIPTUSER@$CHECK_HOST:$SCRIPTFILE" 2>/dev/null; echo $? ` -ne 0 ]; then
			echo "  $WARN : $CHECK_HOST_DISPLAY -> Push du fichier $SCRIPTFILE en echec" | gLog
		elif $VERBOSE; then
			echo "  $OK : $CHECK_HOST_DISPLAY -> Push du fichier $SCRIPTFILE reussi" | gLog
		fi

		# 3: Verification de la configuration SUDO (/opt/operating/bin/OperateAppli.ksh & /bin/md5sum)
		if [ ` su $SCRIPTUSER -c "ssh -nq $SCRIPTUSER@$CHECK_HOST 'sudo /opt/operating/bin/OperateAppli.ksh -h'" >/dev/null 2>&1; echo $? ` -ne 0 ] || [ ` su $SCRIPTUSER -c "ssh -nq $SCRIPTUSER@$CHECK_HOST 'sudo md5sum /opt/operating/bin/OperateAppli.ksh'" >/dev/null 2>&1; echo $? ` -ne 0 ]; then
			echo "  $KO : $CHECK_HOST_DISPLAY -> Probleme de droit SUDO sur /opt/operating/bin/OperateAppli.ksh ou /bin/md5sum" | gLog
			CR_CHECK_HOST=1
		elif $VERBOSE; then
			echo "  $OK : $CHECK_HOST_DISPLAY -> Les droits SUDO sont corrects" | gLog
		fi

		# 4: Verification de l'existence du fichier de configuration cible, et son md5sum
		if [ ` su $SCRIPTUSER -c "ssh -nq $SCRIPTUSER@$CHECK_HOST 'test -f $CONFFILE'" >/dev/null 2>&1; echo $? ` -ne 0 ] || [ "$MD5CONFFILE" != ` su $SCRIPTUSER -c "ssh -nq $SCRIPTUSER@$CHECK_HOST 'sudo md5sum $CONFFILE'" | cut -d' ' -f1 ` ]; then
			echo "  $KO : $CHECK_HOST_DISPLAY -> $CONFFILE absent ou different de celui en local (md5sum)" | gLog
			CR_CHECK_HOST=1
		elif $VERBOSE; then
			echo "  $OK : $CHECK_HOST_DISPLAY -> $CONFFILE est correct" | gLog
		fi

		# 5: Verification de l'existence du script cible, et son md5sum
		if [ ` su $SCRIPTUSER -c "ssh -nq $SCRIPTUSER@$CHECK_HOST 'test -f $SCRIPTFILE'" >/dev/null 2>&1; echo $? ` -ne 0 ] || [ "$MD5SCRIPTFILE" != ` su $SCRIPTUSER -c "ssh -nq $SCRIPTUSER@$CHECK_HOST 'sudo md5sum $SCRIPTFILE'" | cut -d' ' -f1 ` ]; then
			echo "  $KO : $CHECK_HOST_DISPLAY -> $SCRIPTFILE absent ou different de celui en local (md5sum)" | gLog
			CR_CHECK_HOST=1
		elif $VERBOSE; then
			echo "  $OK : $CHECK_HOST_DISPLAY -> $SCRIPTFILE est correct" | gLog
		fi

	done

	# Si mode FORCE, on stocke le hostname pour bypass
	if [ $CR_CHECK_HOST -ne 0 ] && $FORCE; then
		stockBypassHost $CHECK_HOST
	fi

	return $CR_CHECK_HOST
}

# ----------------- Verification des commandes a lancer -----------------------
#
verifCmd() {
	CHECK_CMD="$1"
	CHECK_HOST="$2"
	CHECK_HOST_DISPLAY="${c_jaun}$CHECK_HOST${c_norm}"

	CR_CHECK_CMD=0

	# Verification de l'existence et executabilite du script cible
	if [ "$CHECK_HOST" = "$HOSTNAME" ]; then
		if [ ! -x "$CHECK_CMD" ]; then
			echo "  $KO : $CHECK_HOST_DISPLAY -> $CHECK_CMD n'existe pas ou n'est pas executable" | gLog
			CR_CHECK_CMD=1
		elif $VERBOSE; then
			echo "  $OK : $CHECK_HOST_DISPLAY -> $CHECK_CMD existe et est executable" | gLog
		fi
	else
		if [ ` su $SCRIPTUSER -c "ssh -nq $SCRIPTUSER@$CHECK_HOST 'test -x $CHECK_CMD'"; echo $? ` -ne 0 ]; then
			echo "  $KO : $CHECK_HOST_DISPLAY -> $CHECK_CMD n'existe pas ou n'est pas executable" | gLog
			CR_CHECK_CMD=1
		elif $VERBOSE; then
			echo "  $OK : $CHECK_HOST_DISPLAY -> $CHECK_CMD existe et est executable" | gLog
		fi
	fi

	# Si mode FORCE, on stocke le hostname pour bypass
	if [ $CR_CHECK_CMD -ne 0 ] && $FORCE; then
		stockBypassHost $CHECK_HOST
	fi

	return $CR_CHECK_CMD
}

# ----------------- Lancement d'une etape locale ------------------------------
#
lanceCommande() {
	l_CMD="$1"
	l_RET_OK="$2"
	INT_RETURN=0

	# Un CODE RETOUR specifique est attendu, ou rien de particulier
	if [ -z "$l_RET_OK" ] || [ ` echo "$l_RET_OK" | egrep -c "^([0-9]+|\s*)$" ` -eq 1 ]; then
		if [ -z "$l_RET_OK" ]; then
			# On attends un CR=0 (standard)
			l_RET_OK=0
		fi

		# Si non verbose, on inhibe les sorties
		! $VERBOSE && l_CMD="$l_CMD >/dev/null 2>/dev/null"

		# Lancement de la COMMANDE
		eval $l_CMD
		CMD_CR=$?

		# Verification
		if [ $CMD_CR -eq $l_RET_OK ]; then
			echo "[ $SUCCES ]"
		else
			echo "[ $ECHEC ]"
			INT_RETURN=1
		fi
		
		echo "COMMAND_RETURN=$CMD_CR"
		echo "EXPECTED_RETURN=$l_RET_OK"
	# Une CHAINE DE CARACTERE specifique est attendue
	else
		# Si non verbose, on inhibe les sorties
		if ! $VERBOSE; then
			l_CMD="$l_CMD > $WORKDIR/OperateAppli_$$.tmp 2>/dev/null"
		else
			l_CMD="$l_CMD | tee $WORKDIR/OperateAppli_$$.tmp"
		fi

		# Lancement de la COMMANDE
		eval $l_CMD
		CMD_CR=$?

		# Verification
		if [ ` egrep -ic "$l_RET_OK" $WORKDIR/OperateAppli_$$.tmp ` -gt 0 ]; then
			echo "[ $SUCCES ]"
		else
			echo "[ $ECHEC ]"
			INT_RETURN=1
		fi
		rm -f "$WORKDIR/OperateAppli_$$.tmp"
	fi

	echo "INT_RETURN=$INT_RETURN"
	return $CMD_CR
}

# ----------------- Lancement d'une etape locale ------------------------------
#
lanceEtapeFromSSH() {
	LINENUM="$1"
	SRCMD5CONFFILE="$2"
	SRCMD5SCRIPTFILE="$3"

	# Verification du CKSUM du fichier de configuration entre les serveurs
	if [ "$MD5CONFFILE" != "$SRCMD5CONFFILE" ]; then
		echo "  $KO : MD5SUM du fichier de configuration ${CONFFILE} different entre le serveur source et cible."
		return 10
	fi

	# Verification du CKSUM du script entre les serveurs
	if [ "$MD5SCRIPTFILE" != "$SRCMD5SCRIPTFILE" ]; then
		echo "  $KO : MD5SUM du script${SCRIPTFILE} different entre le serveur source et cible."
		return 11
	fi

	# Recuperation de la ligne dans le fichier de configuration
	ETAPELINE="` sed "${LINENUM}q;d" $CONFFILE `"

	# Verification de l'existence de l'etape dans la configuration
	if [ ` echo $ETAPELINE | egrep -c "^\s*[^:]+:[^:]+:[^:]+:[^:]+:[^:]+:$HOSTNAME(-adm)?:[^:]+:[^:]+:.*$" ` -ne 1 ]; then
		echo "  $KO : Ligne $LINENUM (sur $HOSTNAME) introuvable dans ${CONFFILE} ."
		return 12
	fi

	# Recuperation des variables
	IFS=":"
	set -- $ETAPELINE
	IFS="$ORIGIN_IFS"
	USER="$4"
	COMMANDE="$5"
	RETOUR_OK="$9"

	# Verification de l'existence du script cible par la commande
	SCRIPT="` echo "$COMMANDE" | cut -d' ' -f1 `"
	if [ ! -x "$SCRIPT" ]; then
		echo "  $KO : $SCRIPT n'existe pas ou n'est pas executable."
		return 13
	fi

	# Selon le USER a utiliser
	[ "$USER" != "root" ] && COMMANDE="su - $USER -c \"$COMMANDE\""

	# Lancement de la COMMANDE
	lanceCommande "$COMMANDE 2>&1" "$RETOUR_OK"
	return $?
}

# ----------------- Lancement de l'action demandee ----------------------------
#
lanceAction() {
	ACTION="` echo $1 | tr '[a-z]' '[A-Z]' `"
	GR_FONCT="` echo $2 | tr '[a-z]' '[A-Z]' `"
	VERIFIE=true
	ETAPE=1

	# Cas particulier: GR_FONCT = localhost
	if [ "` echo "$GR_FONCT" | tr '[a-z]' '[A-Z]' `" = "LOCALHOST" ]; then
		GR_FONCT="$HOSTNAME"
	fi

	# Cas particulier: mot cle RESTART -> STOP puis START (a peaufiner)
	if [ "$ACTION" = "RESTART" ]; then
		lanceAction STOP $GR_FONCT
		lanceAction START $GR_FONCT
		return $?
	fi

	echo "  ----------------------------------------------------" | gLog
	echo "  - PREREQUIS" | gLog
	echo "  ----------------------------------------------------" | gLog
	echo | gLog

	# Verification du nombre d'etape trouve avec les parametres donnes
	NB_ETAPES="` egrep -i "^\s*$GR_FONCT:$ACTION:[^:]+:[^:]+:[^:]+:[^:]+:[^:]+:[^:]+:.*$" $CONFFILE | cut -d: -f3 | sort -u | wc -l | tr -d ' ' `"
	if [ $NB_ETAPES -eq 0 ]; then
		echo "  $KO : Aucune ligne trouvee dans le fichier de configuration pour ces parametres : ACTION=$ACTION - GR_FONCT=$GR_FONCT" | gLog
		VERIFIE=false
	fi

	# Verifications de la conformite des HOSTS cibles de l'action
	HOSTLIST="` egrep -i "^\s*$GR_FONCT:$ACTION:[^:]+:[^:]+:[^:]+:[^:]+:[^:]+:[^:]+:.*$" $CONFFILE | cut -d: -f6 | sort -u | grep -v "$HOSTNAME" `"
	verifHostsCibles $HOSTLIST
	[ $? -ne 0 ] && VERIFIE=false

	# Verification de l'existence de tous les scripts qui vont etre a utiliser, distants et locaux
	HOST_CMD_LIST="` egrep -i "^\s*$GR_FONCT:$ACTION:[^:]+:[^:]+:[^:]+:[^:]+:[^:]+:[^:]+:.*$" $CONFFILE | cut -d: -f5,6 `"
	IFS=$(echo -en "\n\b")
	for line in $HOST_CMD_LIST; do
		IFS="$ORIGIN_IFS"
		CHECK_CMD="` echo $line | cut -d: -f1 | cut -d' ' -f1 `"
		CHECK_HOST="` echo $line | cut -d: -f2 `"
		verifCmd $CHECK_CMD $CHECK_HOST
		[ $? -ne 0 ] && VERIFIE=false
	done
	IFS="$ORIGIN_IFS"
	
	# Fin des verifications
		echo | gLog
	if $VERIFIE; then
		echo " $OK : PREREQUIS SATISFAISANTS" | gLog
	elif ! $TESTONLY && ! $FORCE; then
		echo " $KO : PREREQUIS ECHOUES (relancez avec l'option -f si vous souhaitez forcer)" | gLog
		return 20
	else
		echo " $KO : PREREQUIS ECHOUES (mais en mode TEST ou FORCE -> On continue)" | gLog
	fi
	echo | gLog

	CPT_SECURITE=0

	while [ $ETAPE -le $NB_ETAPES ] ; do

		CPT_SECURITE=$(( $COMPTEUR_SECURITE + 1 ))
		CPT_CMD_OK=0
		CPT_CMD_KO=0
		ETAPE_STATUS=
		ETAPE_SUIV=
		TEST_REUSSI=true

		# ----------- DEBUT DE L'ETAPE -----------
		#
		echo "  ----------------------------------------------------" | gLog
		echo "  - ETAPE ${c_cyan}$ETAPE${c_norm} sur ${c_cyan}$NB_ETAPES${c_norm}" | gLog
		echo "  ----------------------------------------------------" | gLog
		echo | gLog

		# Filtre des lignes nous concernant
		F_LINES=` egrep -i "^\s*$GR_FONCT:$ACTION:$ETAPE:[^:]+:[^:]+:[^:]+:[^:]+:[^:]+:.*$" $CONFFILE `

		# Si rien n'a ete trouve, on a termine
		[ -z "$F_LINES" ] && break

		# ----------- VERIFICATIONS -------------
		#
		# Les lignes d'une meme etape doivent toutes avoir la meme prochaine etape
		if [ ` echo "$F_LINES" | cut -d: -f7 | sort -u | wc -l ` -ne 1 ]; then
			echo "  $KO : Les lignes de l'etape ${c_cyan}$ETAPE${c_norm} n'ont pas toutes la meme prochaine etape en cas de succes." | gLog
			ETAPE_CR=21
			# Si pas en mode test, on arrete la
			! $TESTONLY && break
			TEST_REUSSI=false
			
		elif [ ` echo "$F_LINES" | cut -d: -f8 | sort -u | wc -l ` -ne 1 ]; then
			echo "  $KO : Les lignes de l'etape ${c_cyan}$ETAPE${c_norm} n'ont pas toutes la meme prochaine etape en cas d'echec." | gLog
			ETAPE_CR=22
			# Si pas en mode test, on arrete la
			! $TESTONLY && break
			TEST_REUSSI=false
		fi

		# Verifications OK & Mode test --> Etape suivante
		if $TESTONLY; then
			if $TEST_REUSSI; then
				echo "  $OK : ETAPE ${c_cyan}$ETAPE${c_norm} COHERENTE (mode TEST)" | gLog
			else
				echo | gLog
				echo "  $KO : ETAPE ${c_cyan}$ETAPE${c_norm} INCOHERENTE (mode TEST)" | gLog
			fi
			ETAPE=$(( $ETAPE + 1 ))	
			continue
		fi

		ETAPE_CR=0	# Code Retour de l'etape
		i=0		# Index du tableau des PID

		IFS=$(echo -en "\n\b")
		for line in $F_LINES; do
			IFS=":"
			set -- $line
			IFS="$ORIGIN_IFS"
			T_USER="$4"			# User pour lancer la commande
			T_CMD="$5"			# Commande
			T_HOSTNAME="` echo $6 | tr '[A-Z]' '[a-z]'`"	# Hostname cible de la commande
			T_ETAPE_OK="$7"			# Etape si OK
			T_ETAPE_KO="$8"			# Etape si KO
			T_RET_OK="$9"			# Retour attendu

			# On log la COMMANDE, l'USER et l'HOST cible
			echo "${c_jaun}$T_HOSTNAME${c_norm} : $T_CMD (user: $T_USER)" > $WORKDIR/OperateAppli_$$_ETAPE${ETAPE}_CMD${i}.tmp
			echo "----------------------------------------------" >> $WORKDIR/OperateAppli_$$_ETAPE${ETAPE}_CMD${i}.tmp

			# Commande a lancer sur ce serveur
			if [ "$HOSTNAME" = "$T_HOSTNAME" ] || [ "${HOSTNAME}-adm" = "$T_HOSTNAME" ]; then
				# Selon le USER
				[ "$T_USER" != "root" ] && T_CMD="su - $T_USER -c \"$T_CMD\""

				# Lancement de la COMMANDE
				lanceCommande "$T_CMD 2>&1" "$T_RET_OK" >> "$WORKDIR/OperateAppli_$$_ETAPE${ETAPE}_CMD${i}.tmp" &
				# On recupere le PID pour surveillance
				CHILDS_PID[$i]=$!
				i=$(( $i + 1 ))
			
			# Commande a lancer en SSH sur un autre serveur
			else
				# Verification mode FORCE et BYPASS ou non de ce HOST
				if $FORCE && ` checkBypassHost $T_HOSTNAME `; then
					echo "  $T_HOSTNAME -> Bypass (Mode FORCE)." | gLog
					continue
				fi

				# On recupere le numero de ligne dans le fichier de configuration
				LINENUM=` grep -n "$line" $CONFFILE | cut -d: -f1 `
				if [ ` echo "$LINENUM" | egrep -c "^[0-9]+$" ` -ne 1 ]; then
					echo "  $KO : Recuperation de la ligne courante en echec." | gLog
					ETAPE_CR=23
					break
				fi

				# On lance le script distant en fournissant le numero de ligne a jouer et le CKSUM a verifier
				su $SCRIPTUSER -c "ssh -nq $SCRIPTUSER@$T_HOSTNAME 'sudo $SCRIPTFILE $OPTIONS lanceEtapeFromSSH $LINENUM $MD5CONFFILE $MD5SCRIPTFILE'" >> "$WORKDIR/OperateAppli_$$_ETAPE${ETAPE}_CMD${i}.tmp" &
				# On recupere le PID pour surveillance
				CHILDS_PID[$i]=$!
				i=$(( $i + 1 ))
			fi

		done
		IFS="$ORIGIN_IFS"

		# ------ ATTENTE DE FIN DES PROCESS ------
		#
		if [ ${#CHILDS_PID[@]} -gt 1 ]; then
			echo "  ${#CHILDS_PID[@]} commandes lancees en parallele, en attente des PID : ${CHILDS_PID[@]}" | gLog
		elif [ ${#CHILDS_PID[@]} -eq 1 ]; then
			echo "  1 commande lancee, en attente du PID : ${CHILDS_PID[@]}" | gLog
		fi
		echo | gLog

		ETAPE_CR=0

		# Tant qu'il y a des PID dans le tableau
		while (( ${#CHILDS_PID[@]} )); do

			# On verifie que TOUTES les commandes sont terminees
			for j in $( seq 0 $(( ${#CHILDS_PID[@]} - 1 )) ); do

				# Si la COMMANDE est terminee
				if [ ` kill -0 ${CHILDS_PID[$j]} 2>/dev/null; echo $? ` -ne 0 ]; then
					
					# On affiche le contenu texte du fichier
					cat $WORKDIR/OperateAppli_$$_ETAPE${ETAPE}_CMD${j}.tmp 2>&1 | sed -r -e "s/^/  + /" -e "s/SUCCES|DEMARRE/$SUCCES/" -e "s/ECHEC|ARRETE/$ECHEC/" | gLog
					echo "  + ----------------------------------------------" | gLog
					echo | gLog

					# On y recupere le CODE RETOUR
					CR=` egrep "^\s*INT_RETURN=" $WORKDIR/OperateAppli_$$_ETAPE${ETAPE}_CMD${j}.tmp | cut -d= -f2 | cut -d' ' -f1 `
					if [ ` echo $CR | egrep -c "^[0-9]$" ` -ne 1 ] || [ $CR -ne 0 ]; then
						ETAPE_CR=$CR
						CPT_CMD_KO=$(( $CPT_CMD_KO + 1 ))
					else
						CPT_CMD_OK=$(( $CPT_CMD_OK + 1 ))
					fi

					# On supprime ce PID du tableau
					unset "CHILDS_PID[$j]"

					# On affiche les PID restants
					if [ ${#CHILDS_PID[@]} -gt 0 ]; then
						echo "En attente du/des PID restant(s) : ${CHILDS_PID[@]}" | gLog
						echo | gLog
					fi
				fi
			done
			sleep 1	# Pooling de 1 seconde
		done

		# On undefine $CHILDS_PID pour les prochaines utilisations
		unset "CHILDS_PID"

		# ----------- FIN DE L'ETAPE -------------

		# Passage a l'etape suivante
		if ! $STATUS && [ $ETAPE_CR -ne 0 ]; then
			ETAPE_STATUS="${c_roug}KO${c_norm}"
			ETAPE_SUIV=$T_ETAPE_KO
		else
			ETAPE_STATUS="${c_vers}OK${c_norm}"
			ETAPE_SUIV=$T_ETAPE_OK
		fi

		# Cas particulier pour l'affichage : STATUS
		if ! $STATUS; then
			echo "  - COMPTE RENDU DE L'ETAPE ${c_cyan}$ETAPE${c_norm} :" | gLog
			[ $CPT_CMD_OK -gt 0 ] && echo "      $CPT_CMD_OK en $SUCCES" | gLog
			[ $CPT_CMD_KO -gt 0 ] && echo "      $CPT_CMD_KO en $ECHEC" | gLog
			echo | gLog
		else
			echo "  - COMPTE RENDU DE L'ETAPE ${c_cyan}$ETAPE${c_norm} :" | gLog
			[ $CPT_CMD_OK -gt 0 ] && echo " - ....  ${c_vert}$CPT_CMD_OK DEMARRE(S)${c_norm}" | gLog
			[ $CPT_CMD_KO -gt 0 ] && echo " - ....  ${c_roug}$CPT_CMD_KO ARRETE(S) OU MAL DEMARRE(S)${c_norm}" | gLog
			echo | gLog
		fi

		# Traitement des mots cles
		[ "$ETAPE_SUIV" = "END" ] && ETAPE_SUIV=$(( $NB_ETAPES + 1 ))	# Mot cle "END" = Fin
		[ "$ETAPE_SUIV" = "N" ] && ETAPE_SUIV=$(( $ETAPE + 1 ))		# Mot cle "N" = Etape suivante

		# Affichage
		if [ $ETAPE -lt $NB_ETAPES ]; then
			echo "  - ETAPE ${c_cyan}$ETAPE${c_norm} : $ETAPE_STATUS --> GO TO ETAPE ${c_cyan}$ETAPE_SUIV${c_norm}" | gLog
		else
			echo "  - ETAPE ${c_cyan}$ETAPE${c_norm} : $ETAPE_STATUS --> GO TO ${c_jaun}END${c_norm}" | gLog
		fi

		# Prochaine etape
		ETAPE=$ETAPE_SUIV

		# Securite pour eviter boucle infini: max 50 iterations
		[ $CPT_SECURITE -gt 10 ] && break
	done

	return $ETAPE_CR
}

# =============================================================================
# ================= MAIN ======================================================
# =============================================================================
#
# CODE RETOUR :
#	0 : Fin normale

# --------------- Variables de base ---------------
#
VERSION="S0F6"		# Version du script
HOSTNAME="` hostname | cut -d. -f1 | tr "[A-Z]" "[a-z]" `"
ORIGIN_IFS="$IFS"
SCRIPTFILE="` readlink -f $0 `"
WORKDIR="` dirname $SCRIPTFILE `"
CONFFILE="$WORKDIR/OperateAppli.conf"
SCRIPTUSER="4appli"

# --------------- Repertoire de log -------------
#
if [ -d "$WORKDIR/log" ]; then
	LOGDIR="$WORKDIR/log"
elif [ -d "$WORKDIR/logs" ]; then
	LOGDIR="$WORKDIR/logs"
elif [ -d "$WORKDIR/../log" ]; then
	LOGDIR="$WORKDIR/../log"
elif [ -d "$WORKDIR/../logs" ]; then
	LOGDIR="$WORKDIR/../logs"
else
	LOGDIR="$WORKDIR"
fi

LOGFILE="$WORKDIR/OperateAppli.log"
LOCKFILE="${WORKDIR}/OperateAppli.pid"

MD5CONFFILE="` md5sum $CONFFILE | cut -d' ' -f1 `"	# CKSUM du fichier de configuration pour comparaison sur serveur distant
MD5SCRIPTFILE="` md5sum $SCRIPTFILE | cut -d' ' -f1 `"	# CKSUM du script pour comparaison sur serveur distant

# -------- Execution unique ---------------------
#
if [ -e "${LOCKFILE}" ]
then
        # Recuperation du PID d'execution precedente
        PID_LOCK=$( cat "${LOCKFILE}" )
        # Toujours en cours ?
        if [ $(kill -0 ${PID_LOCK} 2>/dev/null; echo $?) -eq 0 ];then
                # Le PID existe toujours, on arrete ici
		echo " UNE EXECUTION EST DEJA EN COURS ! (PID : ${PID_LOCK})"
                exit 0
        else
                # Le PID n'existe plus, on nettoie le fichier
                rm -f "${LOCKFILE}"
        fi
fi

# Mise en place du fichier lock (avec PID)
echo ${BASHPID} > "${LOCKFILE}"

# -------- Traitement des arguments -------------
#
VERBOSE=false	# Mode VERBEUX
COLORS=false	# Mode COULEUR
TESTONLY=false	# Mode TEST
FORCE=false	# Mode FORCE
STATUS=false	# Action speciale STATUS
OPTIONS=""	# Liste des options
nb_bypass=0	# Nombre de host a bypass (si mode FORCE)
#
while getopts "tVcvhf" opt
do
        case $opt in
                 c)     OPTIONS="$OPTIONS -c"; COLORS=true;	;;
                 v)     OPTIONS="$OPTIONS -v"; VERBOSE=true	;;
		 V)	echo "`basename $0` : VERSION=$VERSION"; exit 0	;;
                 t)     OPTIONS="$OPTIONS -t"; TESTONLY=true	;;
                 f)     OPTIONS="$OPTIONS -f"; FORCE=true	;;
                 h)     usage; exit 0	;;
		 *)	usage; exit 1	;;
        esac
done

# On retire les arguments courts traites
shift $((OPTIND-1))

# On doit avoir a minima 2 arguments : ACTION et GR_FONCT
if [ ${#@} -lt 2 ]; then
	usage
	exit 1
fi

# Cas particulier: STATUS
( [ "` echo $1 | tr '[a-z]' '[A-Z]' `" = "STATUS" ] ) && STATUS=true

# -------- Init du formatage --------------------
#
initFormat

# -------- Verification basiques ----------------
#

# Existence du fichier de configuration
if [ ! -f "$CONFFILE" ]; then
	echo "  $KO : Fichier de conf $CONFFILE introuvable." | gLog
	exit 2
fi

# Existance du user 
if [ ` id $SCRIPTUSER >/dev/null 2>&1; echo $? ` -ne 0 ]; then
	echo "  $KO : Utilisateur $SCRIPTUSER introuvable." | gLog
	exit 3
fi

# 5 MB disponibles sur le FS du $WORKDIR
FREESPACE=` df -P $WORKDIR | tail -1 | awk '{ print $4 }' `
if [ $FREESPACE -lt 5 ]; then
	echo "  $KO : Espace libre < 5MB sur le FS de $WORKDIR ($FREESPACE MB)" | gLog
	exit 4
fi

# -------- On traite selon l'action demandee ----
#
case "$1" in
	"lanceEtapeFromSSH")
		# On est appele en SSH pour une action locale
		lanceEtapeFromSSH "$2" "$3" "$4"
		CR=$?
	;;
	*)
		echo | gLog
		echo "=================================================" | gLog
		echo "== DEBUT OPERATE APPLI : ` date '+%d/%m/%Y %T' `" | gLog
		echo "== VERSION : $VERSION" | gLog
		echo "== CMD : ${0##*/} $@" | gLog
		echo "=================================================" | gLog
		echo | gLog
		# On est appele pour derouler toute la procedure
		lanceAction "$1" "$2"
		CR=$?
		echo | gLog
		echo "=================================================" | gLog
		echo "== FIN OPERATE APPLI : ` date '+%d/%m/%Y %T' `" | gLog
		echo "=================================================" | gLog
		echo
	;;
esac

# -------- Fin ----------------------------------
#
# Suppression des fichiers temporaires
find $WORKDIR -name "OperateAppli_*.tmp" -delete

# Suppression du fichier lock (avec PID)
rm -f "${LOCKFILE}"

# Sortie
exit $CR
